<?

define('REDIRECT_URI', 'icon.bitrix24.ru');
define('PROTOCOL', 'https://');


function query($method, $url, $data = null, $jsonDecode = false) // построение запроса к порталу
{
	$curlOptions = array(
		CURLOPT_RETURNTRANSFER => true
	);

	if($method == "POST")
	{
		$curlOptions[CURLOPT_POST] = true;
		$curlOptions[CURLOPT_POSTFIELDS] = http_build_query($data);
	}
	elseif(!empty($data))
	{
		$url .= strpos($url, "?") > 0 ? "&" : "?";
		$url .= http_build_query($data);
	}

	$curl = curl_init($url);
	curl_setopt_array($curl, $curlOptions);
	$result = curl_exec($curl);

	return ($jsonDecode ? json_decode($result, 1) : $result);
}

function call($domain, $method, $params) // выполнение запроса с REST-методом к порталу
{
	return query("POST", PROTOCOL.$domain."/rest/".$method, $params, true);
}

function build_message($data) // получение данных по http из источника и преобразование их в массив для передачи в ленту
{
	$messageArray = array();

	//Выборка нужных данных по переменным
	$dateTmp = $data['comment']['updated_on'];
	// $commitDate = $dateTmp;

	$da = new DateTime("2018-07-17T11:02:48+03:00");
	echo $da->format('d-m-Y H:i:s');
	// $commitDate = new DateTime($data['comment']['updated_on'])->format('Y-m-d H:i:s');
	// date_format($dateTmp, "d.m.Y H:i");//Дата и время комментария
	$repositoryName = $data['repository']['full_name'];//Название репозитория
	$repositoryHref = $data['repository']['links']['html']['href']; //Адрес репозитория
	$commentAutor = $data['actor']['display_name']; //Имя автора

	if (!empty($data['commit'])) {
		$commitText = $data['commit']['message']; //Текст комментария


		$queryData = array('POST_TITLE' => $commentAutor . " закомитил в " . $repositoryName,
	                    'POST_MESSAGE' => $commitText . "\n". $repositoryHref);
		$messageArray[] = $queryData;
	}

	if (!empty($data['push'])) {
		$commitText = $data['push']['changes'][0]['commits'][0]['message']; //Текст комментария


		$queryData = array('POST_TITLE' => $commentAutor . " запушил в " . $repositoryName,
	                    'POST_MESSAGE' => $commitText . "\n". $repositoryHref);
		$messageArray[] = $queryData;
	}

	return $messageArray;
}
?>
