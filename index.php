<?php

$filename = 'main.log';
$curentTime = date("H:i:s");

$method = $_SERVER['REQUEST_METHOD'];
$rawPostData = file_get_contents('php://input');

if (strlen($rawPostData) > 0) {
		$data = json_decode($rawPostData, true); //Это ассоциативный массив

		if (array_key_exists('repository', $data)) {

			require('config.php');

			$text = PHP_EOL . "Запись: " . $curentTime . "\n";
			$text .= PHP_EOL . " Метод " . $method . "\n";
			$text .= PHP_EOL . " SERVER_ADDR " . $_SERVER['SERVER_ADDR'] . "\n";
			$text .= PHP_EOL . " argv " . $_SERVER['argv'] . "\n";
			$text .= PHP_EOL . " QUERY_STRING " . $_SERVER['QUERY_STRING'] . "\n";

			file_put_contents($filename, PHP_EOL . $text, FILE_APPEND);//запись в лог файл

					$params = array(
						"code" => "secret_token",
						"data" => $data
					);
					query("POST", '/webhooks/webhook.php', $params, false);
			}
		}
